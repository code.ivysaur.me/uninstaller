# uninstaller

![](https://img.shields.io/badge/written%20in-Java%20%28Android%29-blue)

An uninstaller app for Android with several different backends.

In certain failure cases Android will be unable to use the regular package manager for APK uninstallation. This app provides a semi-convenient interface allowing app uninstallation via either intent, `pm`, `pm` as root, or manually deleting data files.

The application's recycle bin icon comes from the public domain Tango icon set.


## Download

- [⬇️ uninstaller-1.0.0.src.7z](dist-archive/uninstaller-1.0.0.src.7z) *(101.32 KiB)*
- [⬇️ uninstaller-1.0.0.apk](dist-archive/uninstaller-1.0.0.apk) *(1.02 MiB)*
